const render = (obj, elem) => elem.append(obj.render());

export {render};