import {LoginModal, VisitCardio, VisitDentist, VisitTherapist } from "./components/index.js";

const root = document.getElementById("root");

const loginModalProps = {
    id: "login-modal", 
    form: {id: "login-form"}
};

const loginModal = new LoginModal(loginModalProps);

root.append(loginModal.render());

const openLoginModal = document.getElementById("open-login-modal");
openLoginModal.addEventListener("click", loginModal.open.bind(loginModal));

const visitCardioProps = {
    purpose: 'purpose',
    description: 'description',
    urgency:'high',
    age:'age',
    lastName: 'lastName',
    name: 'name',
    middleName: 'middleName',
    cardId: 12,
    normalPressure: 'normalPressure',
    bodyMassIndex: 'bodyMassIndex',
    prevCardioDiagnosis: 'prevCardioDiagnosis',
};
const visit = new VisitCardio(visitCardioProps);

const visitTherapistProps = {
    purpose: 'purpose',
    description: 'description',
    urgency:'high',
    age:'age',
    lastName: 'lastName',
    name: 'name',
    middleName: 'middleName',
    cardId: 12,
};
const visit = new VisitTherapist(visitTherapistProps);

const visitDentistProps = {
    purpose: 'purpose',
    description: 'description',
    urgency:'high',
    lastName: 'lastName',
    name: 'name',
    middleName: 'middleName',
    cardId: 12,
};
const visit = new VisitDentist(visitDentistProps);

const cardDeck = document.getElementById('card-deck');
cardDeck.append(visit.render());
