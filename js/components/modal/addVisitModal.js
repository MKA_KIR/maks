import {Modal} from "../classes/index.js";
import {SelectDoctorForm} from "../forms/index.js";

class AddVisitModal extends Modal {
    constructor({id}){
        super({id});
    }

    render(){
        this.elem = super.render();
        const loginForm = new SelectDoctorForm();
        const modalBody = this.elem.querySelector(".modal-body");
        modalBody.append(SelectDoctorForm.render());
        
        return this.elem;
    }
}

export {AddVisitModal};