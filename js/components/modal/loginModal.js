import {Modal} from "../classes/index.js";
import {LoginForm} from "../forms/index.js";

class LoginModal extends Modal {
    constructor({id, form}){
        super(id);
        this.formProps = Object.assign(form);
    }

    render(){
        this.elem = super.render();
        const loginForm = new LoginForm(this.formProps);
        const modalBody = this.elem.querySelector(".modal-body");
        modalBody.append(loginForm.render());
        
        return this.elem;
    }
}

export {LoginModal};