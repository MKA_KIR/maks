import Visit from "./visit.js";

class VisitTherapist extends Visit {
    constructor({age, ...args}){
        super(args);
        this.doctorType = 'Терапевт';
        this.age = age;
    }
    
    render(){
        super.render();
        this.elem.insertAdjacentHTML('afterbegin', `<div class="card-header">
            <h4 class="my-0 font-weight-normal">${this.doctorType}</h4>
            </div>`);
        return this.elem
    }
    showMore(e) {
        super.showMore(e);
        const list = this.elem.querySelector('.adition-info');
        list.insertAdjacentHTML('beforeend', `<li>Возраст: ${this.age}</li>`)
    }
}

export {VisitTherapist};