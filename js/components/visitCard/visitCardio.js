import Visit from "./visit.js";

class VisitCardio extends Visit {
    constructor({normalPressure, bodyMassIndex, prevCardioDiagnosis,age, ...args}){
        super(args);
        this.doctorType = 'Кардиолог';
        this.normalPressure = normalPressure;
        this.bodyMassIndex = bodyMassIndex;
        this.prevCardioDiagnosis = prevCardioDiagnosis;
        this.age = age;
    }
    
    render(){
        super.render();
        this.elem.insertAdjacentHTML('afterbegin', `<div class="card-header">
            <h4 class="my-0 font-weight-normal">${this.doctorType}</h4>
            </div>`)
        return this.elem
    }
    showMore(e) {
        super.showMore(e);
        const list = this.elem.querySelector('.adition-info');
        list.insertAdjacentHTML('beforeend', `<li>Возраст: ${this.age}</li>
                                                            <li>Обычное давление: ${this.normalPressure}</li>
                                                            <li>Индекс массы тела: ${this.bodyMassIndex}</li>
                                                            <li>Перенесенные заболевания сердечно-сосудистой системы: ${this.prevCardioDiagnosis}</li>`)
}
}

export {VisitCardio};