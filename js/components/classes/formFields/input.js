class Input {
    constructor({type = "text", id = "", name, placeholder = "", value = "", required = false, label = ""}) {
        this.type = type;
        this.className = "form-control";
        this.id = id;
        this.name = name;
        this.placeholder = placeholder;
        this.value = value;
        this.required = required;
        this.label = label;
        this.elem = null;
    }

    render(){
        const inputContainer = document.createElement("div");
        inputContainer.classname = "form-group";
        let label = "";
        if(this.label) {
            const labelFor = this.id || "";
            label = `<label for="${labelFor}">Ваш email</label>`;
        }

        inputContainer.innerHTML = ` ${label}
                                    <input type="${this.type}" class="${this.className}" id="${this.id}" name="${this.name}" placeholder="${this.placeholder}" value="${this.value}" required="${this.required}">`;
        return inputContainer;
    }
}

export {Input};
