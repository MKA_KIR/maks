class Form {
    constructor({id, displayType = ""}){
        this.id = id;
        this.className = (displayType === "inline") ? "form-inline" : "";
        
    }
    
    render(){
        const elem = document.createElement("form");
        elem.className = this.className;
        elem.id = this.id;
        return elem;
    }

    serializeJSON(){
        const body = {};
        const eachField = [...this.elem.querySelectorAll('input[name], select[name], textarea[name]')];
        const notEmptyFields = eachField.filter(({value}) => value);
        notEmptyFields.forEach(({name, value}) => body[name] = value);
        return body;
    }
}

export {Form};