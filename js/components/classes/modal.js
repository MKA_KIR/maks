

class Modal {
    constructor(id) {
        this.id = id;
        this.className = "modal fade";
    }
    
    render(){
        const elem = document.createElement("div");
        elem.id = this.id;
        elem.className = this.className;
        elem.insertAdjacentHTML("beforeend", `<div class="modal-dialog">
        <div class="modal-content">
    
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Modal Heading</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
    
          <!-- Modal body -->
          <div class="modal-body">
            Modal body..
          </div>
    
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>
    
        </div>
      </div>`);
        const closeBtn = elem.querySelector(".close");
        closeBtn.addEventListener("click", this.close.bind(this));
        return elem;
    }
    
    open(e) {
        e.preventDefault();
        this.elem.classList.add("show");
    }
    
    close() {
        this.elem.classList.remove("show");
    }
}

export {Modal};