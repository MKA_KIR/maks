import {Form, Input} from "../classes/index.js";

class LoginForm extends Form {
    constructor({id, displayType = ""}) {
        super({id, displayType: ""});
    }
    
    render(){
        this.elem = super.render();
        const emailProps = {
            type: "email",
            name: "email",
            placeholder: "Введите ваш email",
            labelText: "Email"
        };
        const email = new Input(emailProps);
        
        const passwordProps = {
            type: "password",
            name: "password",
            placeholder: "Введите ваш пароль",
            labelText: "Password"
        };
        const password = new Input(passwordProps);
        this.elem.append(email.render(), password.render());
        
        this.elem.addEventListener("submit", this.handleSubmit.bind(this));
        return this.elem;
        
    }
    
    async handleSubmit(e){
        e.preventDefault();
        const body = this.serializeJSON();

//        const {data} = req.post(this.action, body);
//        if(data.status === "Success") {
//            localStorage.setItem("token", data.token);
//        }
//        else {
//            
//        }
    }
}

export {LoginForm};